#!/bin/bash
# This script runs garnet for a range of values and extracts the value of average latency from 
# stats.txt file, and line by line stores these values in a text file. 
echo > aa.txt
b=0.02
for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
do
    x=$(echo "$i * $b" | bc )
    #echo $x
    str="./build/NULL/gem5.debug configs/example/garnet_synth_traffic.py  --num-cpus=4 --num-dirs=4 --network=garnet2.0 --topology=Mesh_XY --mesh-rows=2  --sim-cycles=1000 --inj-vnet=0 --injectionrate=$x --synthetic=uniform_random"
    eval "$str"
    grep "average_packet_latency" m5out/stats.txt | sed 's/system.ruby.network.average_packet_latency\s*//' >> aa.txt
done
    
