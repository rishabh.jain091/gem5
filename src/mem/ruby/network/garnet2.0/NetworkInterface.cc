/*
 * Copyright (c) 2008 Princeton University
 * Copyright (c) 2016 Georgia Institute of Technology
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Niket Agarwal
 *          Tushar Krishna
 */


#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>

#include "base/cast.hh"
#include "base/stl_helpers.hh"
#include "debug/RubyNetwork.hh"
#include "mem/ruby/network/MessageBuffer.hh"
#include "mem/ruby/network/garnet2.0/Credit.hh"
#include "mem/ruby/network/garnet2.0/NetworkInterface.hh"
#include "mem/ruby/network/garnet2.0/flitBuffer.hh"
#include "mem/ruby/slicc_interface/Message.hh"

using namespace std;
using m5::stl_helpers::deletePointers;

NetworkInterface::NetworkInterface(const Params *p)
    : ClockedObject(p), Consumer(this), m_id(p->id),
      m_virtual_networks(p->virt_nets), m_vc_per_vnet(p->vcs_per_vnet),
      m_num_vcs(m_vc_per_vnet * m_virtual_networks),
      m_deadlock_threshold(p->garnet_deadlock_threshold),
      vc_busy_counter(m_virtual_networks, 0)
{
    m_router_id = -1;
    m_vc_round_robin = 0;
    m_ni_out_vcs.resize(m_num_vcs);
    // m_num_vcs is total number of virtual channels in one node of NoC(counting all VNET's ka VC's)
    m_ni_out_vcs_enqueue_time.resize(m_num_vcs);
    outCreditQueue = new flitBuffer();
    cout << "constructor of NI : instantiating NI flit buffers for node: " << m_id<< " and router: "<<m_router_id<< endl;
    // instantiating the NI flit buffers
    for (int i = 0; i < m_num_vcs; i++) {
        m_ni_out_vcs[i] = new flitBuffer();//creating input flit buffers for each VC
        m_ni_out_vcs_enqueue_time[i] = Cycles(INFINITE_);
    }

    m_vc_allocator.resize(m_virtual_networks); // 1 allocator per vnet
    //one allocator will allocate vcs_per_vnet ()
    for (int i = 0; i < m_virtual_networks; i++) {
        m_vc_allocator[i] = 0; // 0 means no VCs allocated
    }

    m_stall_count.resize(m_virtual_networks);
}

void
NetworkInterface::init()
{
    for (int i = 0; i < m_num_vcs; i++) {
        m_out_vc_state.push_back(new OutVcState(i, m_net_ptr));
    }

    cout << "initializing the NI, initializing VCs with IDLE state for node: " << m_id<< " and router: "<<m_router_id<< endl;


    // id of virtual channel is like this: for 4 vc: 0 .. 1 .. 2 .. 3
    // here 'i' is the id
    // for each router, we have to call this to generate VC with state beingIDLE

}

NetworkInterface::~NetworkInterface()
{
    deletePointers(m_out_vc_state);
    deletePointers(m_ni_out_vcs);
    delete outCreditQueue;
    delete outFlitQueue;
}

void
NetworkInterface::addInPort(NetworkLink *in_link,
                              CreditLink *credit_link)
{   //this will be called when setting up EXT_OUT_ link between NI and router
    // a NI has input port which has in network link: as flits are
    // incoming from the router; has out credit link: as
    // NI sends its credits count to the router to tell available flit space.
    inNetLink = in_link;
    in_link->setLinkConsumer(this); // the NI will consume the flits passed by router
    outCreditLink = credit_link;
    credit_link->setSourceQueue(outCreditQueue);

}

void
NetworkInterface::addOutPort(NetworkLink *out_link,
                             CreditLink *credit_link,
                             SwitchID router_id)
{
   //this will be called when setting up EXT_IN_ link between NI and router
   // a NI has output port which has outNetLink: as flits are outgoing from NI
   // to the router. had inCreditLink.. as router sends the number of credits
   // available via this link.
    inCreditLink = credit_link;
    credit_link->setLinkConsumer(this);

    outNetLink = out_link;
    outFlitQueue = new flitBuffer(); //every ouptut port has a queue
    out_link->setSourceQueue(outFlitQueue);

    m_router_id = router_id;

}

// here node means a controller (directory or cache coherence protocol)
// a node deals with message buffer only.
void
NetworkInterface::addNode(vector<MessageBuffer *>& in,
                            vector<MessageBuffer *>& out)
{
    inNode_ptr = in;
    outNode_ptr = out;
    cout << "adding node for node: " << m_id<< " and router: "<<m_router_id<< endl;
    for (auto& it : in) {
        if (it != nullptr) {
            it->setConsumer(this);
        }
    }
}

void
NetworkInterface::dequeueCallback()
{
    // An output MessageBuffer has dequeued something this cycle and there
    // is now space to enqueue a stalled message. However, we cannot wake
    // on the same cycle as the dequeue. Schedule a wake at the soonest
    // possible time (next cycle).
    scheduleEventAbsolute(clockEdge(Cycles(1)));
}

void
NetworkInterface::incrementStats(flit *t_flit)
{
    int vnet = t_flit->get_vnet();
    std::cout<<">>> <<< Value of vnet is "<< vnet<<" with flit id: "<<t_flit->get_id()<< std::endl;

    // Latency
    m_net_ptr->increment_received_flits(vnet);
    Cycles network_delay =
        t_flit->get_dequeue_time() - t_flit->get_enqueue_time() - Cycles(1);
    Cycles src_queueing_delay = t_flit->get_src_delay();
    Cycles dest_queueing_delay = (curCycle() - t_flit->get_dequeue_time());
    Cycles queueing_delay = src_queueing_delay + dest_queueing_delay;

    m_net_ptr->increment_flit_network_latency(network_delay, vnet);
    m_net_ptr->increment_flit_queueing_latency(queueing_delay, vnet);

    //bookkeeping: when last flit is received
    if (t_flit->get_type() == TAIL_ || t_flit->get_type() == HEAD_TAIL_) {
        m_net_ptr->increment_received_packets(vnet);
        m_net_ptr->increment_packet_network_latency(network_delay, vnet);
        m_net_ptr->increment_packet_queueing_latency(queueing_delay, vnet);
        std::cout << ">>>>>> <<<<<< Packet delay is  "<< network_delay<< std::endl;
        if(t_flit->get_vc() == 0 ){
        //writing latency of packet in a file
        std::ofstream outfile;
        outfile.open("latency.txt", std::ios_base::app);
        outfile << network_delay;
        outfile<< std::endl;
        }

    }

    // Hops
    m_net_ptr->increment_total_hops(t_flit->get_route().hops_traversed);
    std::cout <<">>> <<< This flit took "<<t_flit->get_route().hops_traversed<<"hops!"<< std::endl;
    std::cout << " "<< std::endl;


}

/*
 * The NI wakeup checks whether there are any ready messages in the protocol
 * buffer. If yes, it picks that up, flitisizes it into a number of flits and
 * puts it into an output buffer and schedules the output link. On a wakeup
 * it also checks whether there are flits in the input link. If yes, it picks
 * them up and if the flit is a tail, the NI inserts the corresponding message
 * into the protocol buffer. It also checks for credits being sent by the
 * downstream router.
 */

void
NetworkInterface::wakeup()
{
    DPRINTF(RubyNetwork, "Network Interface %d connected to router %d "
            "woke up at time: %lld\n", m_id, m_router_id, curCycle());
    printf("NI %d connected to router %d, woke up at time ",
                                    m_id, m_router_id);
    cout << curCycle() << endl;
    // adding print statement to know basic details like total vnets, total vcs, vc_per_vnet?
    //cout << "Total VC's are: "<<m_num_vcs << endl; //12
    //cout << "Total VNET's are:  "<< m_virtual_networks<< endl;//3
    //cout << "VC_per_VNET is:  "<<m_vc_per_vnet << endl;//4
    MsgPtr msg_ptr;
    Tick curTime = clockEdge();
    //cout << "wakeup() of NI for node: " << m_id<< " and router: "<<m_router_id<< endl;
    // Checking for messages coming from the protocol buffer(Garnet_standalone cache controller)
    // can pick up a message/cycle for each virtual net
    //and each vnet will have separate message buffer
    // ### transfer of message from NI protocol buffer to VNET buffer ###
    //inNode_ptr is a pointer to an array of message buffers for a NI
    //NOTE - each inNode_ptr message buffer maps to one vnet (size of array being no. of message class)


    for (int vnet = 0; vnet < inNode_ptr.size(); ++vnet) {
        MessageBuffer *b = inNode_ptr[vnet];//checking for message buffers in round robin manner
        if (b == nullptr) {
            continue; //if message buffer is empty, look for next buffer
        }
        //if message is preset and waiting, get the message, convert to flits
        //then, deque the message buffer
        if (b->isReady(curTime)) { // (1)Is there a message waiting
            msg_ptr = b->peekMsgPtr(); //(2)grab the message
            if (flitisizeMessage(msg_ptr, vnet)) { //(3) convert to flits and insert flits present in VC ka buffer
                cout << "converted a message to flits at NI: "<< m_id <<"at time: " <<curCycle()<<endl;
                b->dequeue(curTime);//deque the protocol message buffer
            }
        }
    }
    //DEBUGGING:
    //check the size of inNode_ptr and outNode_ptr..COMES TO 3
    //cout << "Size of inNode_ptr is: "<<inNode_ptr.size() << " indicates no. of VNETS! "<< endl;
    //cout << "Size of outNode_ptr is: "<<outNode_ptr.size()<<" may indicate no. of VNETS" << endl;
    scheduleOutputLink();
    checkReschedule();

    // Check if there are flits stalling a virtual channel. Track if a
    // message is enqueued to restrict ejection to one message per cycle.
    bool messageEnqueuedThisCycle = checkStallQueue();

    /*********** Check the incoming flit link **********/
    if (inNetLink->isReady(curCycle())) {
        flit *t_flit = inNetLink->consumeLink();//EXT_OUT_ external link gives flit
        int vnet = t_flit->get_vnet();
        t_flit->set_dequeue_time(curCycle()); //flit is getting consumed/ejected here
        //printing the debugging details of a flit
        t_flit->print(cout);

        // If a tail flit is received, enqueue into the protocol buffers if
        // space is available. Otherwise, exchange non-tail flits for credits.
        if (t_flit->get_type() == TAIL_ || t_flit->get_type() == HEAD_TAIL_) {
            if (!messageEnqueuedThisCycle &&
                outNode_ptr[vnet]->areNSlotsAvailable(1, curTime)) {
                // Space is available. Enqueue to protocol buffer.
                outNode_ptr[vnet]->enqueue(t_flit->get_msg_ptr(), curTime,
                                           cyclesToTicks(Cycles(1)));

                // Simply send a credit back since we are not buffering
                // this flit in the NI
                sendCredit(t_flit, true);

                // Update stats and delete flit pointer
                incrementStats(t_flit);
                delete t_flit;
            } else {
                // No space available- Place tail flit in stall queue and set
                // up a callback for when protocol buffer is dequeued. Stat
                // update and flit pointer deletion will occur upon unstall.
                m_stall_queue.push_back(t_flit);
                m_stall_count[vnet]++;

                auto cb = std::bind(&NetworkInterface::dequeueCallback, this);
                outNode_ptr[vnet]->registerDequeueCallback(cb);
            }
        } else {
            // Non-tail flit. Send back a credit but not VC free signal.
            sendCredit(t_flit, false);

            // Update stats and delete flit pointer.
            incrementStats(t_flit);
            delete t_flit;
        }
    }

    /****************** Check the incoming credit link *******/

    if (inCreditLink->isReady(curCycle())) {
        Credit *t_credit = (Credit*) inCreditLink->consumeLink();
        m_out_vc_state[t_credit->get_vc()]->increment_credit();
        if (t_credit->is_free_signal()) {
            m_out_vc_state[t_credit->get_vc()]->setState(IDLE_, curCycle());
        }
        delete t_credit;
    }


    // It is possible to enqueue multiple outgoing credit flits if a message
    // was unstalled in the same cycle as a new message arrives. In this
    // case, we should schedule another wakeup to ensure the credit is sent
    // back.
    if (outCreditQueue->getSize() > 0) {
        outCreditLink->scheduleEventAbsolute(clockEdge(Cycles(1)));
    }
}

void
NetworkInterface::sendCredit(flit *t_flit, bool is_free)
{
    Credit *credit_flit = new Credit(t_flit->get_vc(), is_free, curCycle());
    outCreditQueue->insert(credit_flit);
}

bool
NetworkInterface::checkStallQueue()
{
    bool messageEnqueuedThisCycle = false;
    Tick curTime = clockEdge();

    if (!m_stall_queue.empty()) {
        for (auto stallIter = m_stall_queue.begin();
             stallIter != m_stall_queue.end(); ) {
            flit *stallFlit = *stallIter;
            int vnet = stallFlit->get_vnet();

            // If we can now eject to the directory buffer, send back credits
            if (outNode_ptr[vnet]->areNSlotsAvailable(1, curTime)) {
                outNode_ptr[vnet]->enqueue(stallFlit->get_msg_ptr(), curTime,
                                           cyclesToTicks(Cycles(1)));

                // Send back a credit with free signal now that the VC is no
                // longer stalled.
                sendCredit(stallFlit, true);

                // Update Stats
                incrementStats(stallFlit);

                // Flit can now safely be deleted and removed from stall queue
                delete stallFlit;
                m_stall_queue.erase(stallIter);
                m_stall_count[vnet]--;

                // If there are no more stalled messages for this vnet, the
                // callback on it's MessageBuffer is not needed.
                if (m_stall_count[vnet] == 0)
                    outNode_ptr[vnet]->unregisterDequeueCallback();

                messageEnqueuedThisCycle = true;
                break;
            } else {
                ++stallIter;
            }
        }
    }

    return messageEnqueuedThisCycle;
}

// Embed the protocol message into flits
bool
NetworkInterface::flitisizeMessage(MsgPtr msg_ptr, int vnet)
{
    Message *net_msg_ptr = msg_ptr.get();
    NetDest net_msg_dest = net_msg_ptr->getDestination();

    // gets all the destinations associated with this message.
    vector<NodeID> dest_nodes = net_msg_dest.getAllDest();

    // Number of flits is dependent on the link bandwidth available.
    // Link bandwidth is expressed in terms of bytes/cycle or the flit size
    //num_flits is size of message in flits (and flit size is in Bytes)
    int num_flits = (int) ceil((double) m_net_ptr->MessageSizeType_to_int(
        net_msg_ptr->getMessageSize())/m_net_ptr->getNiFlitSize());

    // loop to convert all multicast messages into unicast messages
    for (int ctr = 0; ctr < dest_nodes.size(); ctr++) {

        // this will return a free output virtual channel
        int vc = calculateVC(vnet);

        if (vc == -1) {
            return false ;
        }
        MsgPtr new_msg_ptr = msg_ptr->clone();
        NodeID destID = dest_nodes[ctr];

        Message *new_net_msg_ptr = new_msg_ptr.get();
        if (dest_nodes.size() > 1) {
            NetDest personal_dest;
            for (int m = 0; m < (int) MachineType_NUM; m++) {
                if ((destID >= MachineType_base_number((MachineType) m)) &&
                    destID < MachineType_base_number((MachineType) (m+1))) {
                    // calculating the NetDest associated with this destID
                    personal_dest.clear();
                    personal_dest.add((MachineID) {(MachineType) m, (destID -
                        MachineType_base_number((MachineType) m))});
                    new_net_msg_ptr->getDestination() = personal_dest;
                    break;
                }
            }
            net_msg_dest.removeNetDest(personal_dest);
            // removing the destination from the original message to reflect
            // that a message with this particular destination has been
            // flitisized and an output vc is acquired
            net_msg_ptr->getDestination().removeNetDest(personal_dest);
        }

        // Embed Route into the flits
        // NetDest format is used by the routing table
        // Custom routing algorithms just need destID
        RouteInfo route;
        route.vnet = vnet;
        route.net_dest = new_net_msg_ptr->getDestination();
        route.src_ni = m_id;
        route.src_router = m_router_id;
        route.dest_ni = destID;
        route.dest_router = m_net_ptr->get_router_id(destID);

        // initialize hops_traversed to -1
        // so that the first router increments it to 0
        route.hops_traversed = -1;

        m_net_ptr->increment_injected_packets(vnet);
        for (int i = 0; i < num_flits; i++) {
            m_net_ptr->increment_injected_flits(vnet);
            flit *fl = new flit(i, vc, vnet, route, num_flits, new_msg_ptr,
                curCycle());

            fl->set_src_delay(curCycle() - ticksToCycles(msg_ptr->getTime()));
            m_ni_out_vcs[vc]->insert(fl);
        }

        m_ni_out_vcs_enqueue_time[vc] = curCycle();
        m_out_vc_state[vc]->setState(ACTIVE_, curCycle());
    }
    return true ;
}

// Looking for a free output vc
int
NetworkInterface::calculateVC(int vnet)
{
    for (int i = 0; i < m_vc_per_vnet; i++) {
        int delta = m_vc_allocator[vnet]; //offset to locate free VC id
        m_vc_allocator[vnet]++;
        if (m_vc_allocator[vnet] == m_vc_per_vnet)
            m_vc_allocator[vnet] = 0;
        // id of VC is determined by [(vnet*m_vc_per_vnet) + delta]
        // check if the VC is idle
        if (m_out_vc_state[(vnet*m_vc_per_vnet) + delta]->isInState(
                    IDLE_, curCycle())) {
            // it counts the number of cycles a VNET is busy
            vc_busy_counter[vnet] = 0;
            return ((vnet*m_vc_per_vnet) + delta);
        }
    }
    // after checking all the VCs: increase the busy counter of VNET as
    // no free VC is present
    //Also, if for a long time : no VC becomes free : it means the flits
    // are not getting a free output port for a long time due to (case1)
    //contention with other VNET flits or (case2)downstream router's VC
    // are occupied too for a long time.
    vc_busy_counter[vnet] += 1; // bookkeeping !
    panic_if(vc_busy_counter[vnet] > m_deadlock_threshold,
        "%s: Possible network deadlock in vnet: %d at time: %llu \n",
        name(), vnet, curTick());

    return -1; // if no VC is found, return -1
}


/** This function looks at the NI buffers(VNET's)
 *  if some buffer has flits which are ready to traverse the link in the next
 *  cycle, and the downstream output vc associated with this flit has buffers
 *  left, the link is scheduled for the next cycle
 * NOTE: here we are seeking to transfer flits from NI to router's input flit buffers of VC
 *
 */

void
NetworkInterface::scheduleOutputLink()
{
    int vc = m_vc_round_robin;
    /*so, let's say that we have 4 vc's : id will be : 0, 1, 2 , 3
    *starting from vc = -1: temporary vc values will be -1, 0 ,1 ,2
    * do vc++ to increment,
    * so, when vc = 3, vc++ makes 4: that means we have iterated for all vc's
    */
    //iterate over all vcs in a node, to schedule buffered VC for output port
    for (int i = 0; i < m_num_vcs; i++) {
        vc++;
        if (vc == m_num_vcs)
            vc = 0;

        // model buffer backpressure
        /*
        *(1) for a vc, check the
        */

       //DEBUGGING
       //check the size of m_ni_out_vcs, should be same as m_num_vcs(12)
       //check the size of m_out_vc_state, should be same as m_num_vcs(12)
       //cout << "size of m_ni_out_vcs is : "<<m_ni_out_vcs.size()<< endl;
       //cout << "size of m_out_vc_state is: " <<m_out_vc_state.size()<< endl;
        if (m_ni_out_vcs[vc]->isReady(curCycle()) &&
            m_out_vc_state[vc]->has_credit()) {//check if next vc has available buffers

            /*
            * for our system having 3 vnets, 4 vc_per_vnet
            * 0 1 2 3 -> 0 , 4 5 6 7 -> 1 , 8 9 10 11 -> 2
            * so, for VNETs 0 , 1 , 2 base_vc is : 0, 4, 8
            */

            bool is_candidate_vc = true;
            int t_vnet = get_vnet(vc); //get vnet id for present vc
            int vc_base = t_vnet * m_vc_per_vnet;//starting VC id for VNET

            /*
            *Point to point ordering of flits is important for functional correctness
            *of packets. The order in which packets are generated, should be recieved in
            *the same order. To ensure this: (1)DOR (dim ordered routing)via Mesh_XY
            *(2) using FIFO/queue arbiters for switch arbitration
            */

            /*
            How ordering is done ? or How the VC which got enqueued first is scheduled first?
            *So, first we start with vc = 0
            (2) find the vnet for that vc
            (3)iterate over all VC of that vnet to search if other VC is ready with flits and
            has enqueued eariler
            NotE: this ensures ordering of flits: flits enqueued earlier are scheduled
            before flits enqueued later.
            (4) Schedule lowest enqueued time VC first
            */
            if (m_net_ptr->isVNetOrdered(t_vnet)) {//iterate over all the VC for that VNET
                for (int vc_offset = 0; vc_offset < m_vc_per_vnet;
                     vc_offset++) {
                    int t_vc = vc_base + vc_offset;
                    if (m_ni_out_vcs[t_vc]->isReady(curCycle())) {
                        //compare the enque time to choose right VC
                        if (m_ni_out_vcs_enqueue_time[t_vc] <
                            m_ni_out_vcs_enqueue_time[vc]) {
                            is_candidate_vc = false;
                            break;//an earlier enqueued VC exists, so give chance to it
                        }
                    }
                }
            }
            if (!is_candidate_vc)
                continue;//move to next vc_id till you reach the earliest enqueued VC in the vnet

            m_vc_round_robin = vc; // vc chosen for scheduling

            m_out_vc_state[vc]->decrement_credit();
            //NI should decrement credit count when flit leaves
            // Just removing the flit
            flit *t_flit = m_ni_out_vcs[vc]->getTopFlit(); //returns the top flit by dequeuing the flitbuffer for that VC
            t_flit->set_time(curCycle() + Cycles(1));
            outFlitQueue->insert(t_flit);
            // schedule the out link
            outNetLink->scheduleEventAbsolute(clockEdge(Cycles(1)));

            if (t_flit->get_type() == TAIL_ ||
               t_flit->get_type() == HEAD_TAIL_) {
                m_ni_out_vcs_enqueue_time[vc] = Cycles(INFINITE_);
            }
            return;
        }
    }
}

int
NetworkInterface::get_vnet(int vc)
{
    for (int i = 0; i < m_virtual_networks; i++) {
        if (vc >= (i*m_vc_per_vnet) && vc < ((i+1)*m_vc_per_vnet)) {
            return i;
        }
    }
    fatal("Could not determine vc");
}


// Wakeup the NI in the next cycle if there are waiting
// messages in the protocol buffer, or waiting flits in the
// output VC buffer
void
NetworkInterface::checkReschedule()
{
    for (const auto& it : inNode_ptr) {
        if (it == nullptr) {
            continue;
        }

        while (it->isReady(clockEdge())) { // Is there a message waiting
            scheduleEvent(Cycles(1));
            return;
        }
    }

    for (int vc = 0; vc < m_num_vcs; vc++) {
        if (m_ni_out_vcs[vc]->isReady(curCycle() + Cycles(1))) {
            scheduleEvent(Cycles(1));
            return;
        }
    }
}

void
NetworkInterface::print(std::ostream& out) const
{
    out << "[Network Interface]";
}

uint32_t
NetworkInterface::functionalWrite(Packet *pkt)
{
    uint32_t num_functional_writes = 0;
    for (unsigned int i  = 0; i < m_num_vcs; ++i) {
        num_functional_writes += m_ni_out_vcs[i]->functionalWrite(pkt);
    }

    num_functional_writes += outFlitQueue->functionalWrite(pkt);
    return num_functional_writes;
}

NetworkInterface *
GarnetNetworkInterfaceParams::create()
{
    return new NetworkInterface(this);
}
