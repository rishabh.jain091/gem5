### gem5 ###
1. The [gem5](http://gem5.org/Main_Page) simulator is a modular platform for computer-system architecture research, encompassing system-level architecture as well as processor microarchitecture.

2. [gem5 runs](http://gem5.org/Running_gem5)  in two modes: 
>1. SE mode (Syscall emulation): can simulate user space only programs where system services are provided directly by the simulator
>2. FS mode (Full System): can simulate a complete system with devices and an operating system

3. A detailed description of source code is available at http://gem5.org/Source_Code

4. Details on supported platforms and dependencies: http://gem5.org/Dependencies

5. [Build system](http://gem5.org/Build_System)  of gem5 is based on SCons(an open source build system implemented in python)

###Setup for gem5 development environment ###

1 . gem5  uses python2.7(Frontend) and C++(Backend) 

2 . Installing required dependencies: 

```bash
sudo apt install build-essential git m4 scons zlib1g zlib1g-dev libprotobuf-dev protobuf-compiler libprotoc-dev libgoogle-perftools-dev python-dev python
```

3 . Getting the source code: 
```bash
git clone https://gem5.googlesource.com/public/gem5
cd gem5
```
4 . Building gem5 for RISCV: 
```bash
scons build/RISCV/gem5.opt -j9
```
>1. gem5 uses the scons build system which is based on python. 
>2. With '-j', we can pass number of cores to use for building ISA. (for n cores, pass 'n+1' value )
>3. Target is of the form **build/config/binary** where *config* is replaced with one of the predefined set of build parameters and *binary* is replaced with one of the possible m5 binary names. 
>4. Binaries available for simulation: gem5.opt, gem5.perf, gem5.fast, gem5.debug, gem5.prof 
>5. More details at http://gem5.org/Introduction

5 . Directory structure: 
>The basic source release includes these subdirectories:
   - configs: example simulation configuration scripts
   - ext: less-common external packages needed to build gem5
   - src: source code of the gem5 simulator
   - system: source for some optional system software for simulated systems
   - tests: regression tests
   - util: useful utility programs and files

6 . **System Call Emulation Mode:** 
>1. In this mode, one only needs to specify the binary file to be simulated. This binary file can be statically/dynamically linked.
>2.  configs/examples/se.py is used for configuring and running simulations in this mode.
>3. The binary file to simulated is specified with option -c 
>4. Example command: 
```bash
$ ./build/RISCV/gem5.opt ./configs/example/se.py -c ./tests/test-progs/hello/bin/riscv/linux/hello 
```
7 . The python script for a  simple multi_core setup : two cores with each core having its private L1-I cache, L1-D cache and shared L2 cache is availabe at configs/learning_gem5/part1/two_core.py
To run this : 
```bash
build/RISCV/gem5.debug configs/learning_gem5/part1/two_level_copy.py 
```
NOTE : one can edit the path of executables/binary for each core in this script.

### Simulation of NoC with synthetic traffic using Garnet ###

1 . [Garnet Synthetic Traffic](http://gem5.org/Garnet_Synthetic_Traffic)  is useful for network testing/debugging or for network-only simulations with synthetic traffic. 

2 . It only works with the [Garnet_standalone](http://gem5.org/Garnet_standalone)  coherence protocol which is a dummy cache coherence protocol that is used to operate Garnet in a standalone manner. 

3 . This protocol assumes only 3 vnets(Virtual Networks) and a 1-level cache hierarchy.

4 . To build gem5 with Garnet_standalone coherence protocol: 
```bash
scons build/NULL/gem5.debug PROTOCOL=Garnet_standalone
``` 
The Garnet_standalone protocol is ISA-agnostic, and hence we build it with the NULL ISA. 

 5 . To run garnet synthetic traffic for a 4*4 mesh topology having 16 cpus, 16 directories, uniform random synthetic traffic, simulation cycles of 1000 and 0.01 injection rate: 
 
```bash
./build/NULL/gem5.debug configs/example/garnet_synth_traffic.py --num-cpus=16 --num-dirs=16 --network=garnet2.0 --topology=Mesh_XY --mesh-rows=4  --sim-cycles=1000 --synthetic=uniform_random --injectionrate=0.01
```
>More details about [parameterized options](http://gem5.org/Garnet_Synthetic_Traffic) . 

6 . Details about structure of simulated system is provided in m5out/config.ini . Details about statistics of simulated system is stored in m5out/stats.txt

7 . The code to generate a log of recieved packet latency is present in NetworkInterface.cc. It creates a new file 'latency.txt'  having latency data for each recieved packet. 
(For manually printing log on console, edit the NetworkInterface.cc file and again build the model) 
To extract the values and plot a histogram, run :
```bash
python3 hist.py
```
8 . Presently, everytime the garnet runs: latency.txt file gets appended with packet_network_latency (packet network_delay) values. This file is read by 'hist.py' by extracting latency values line by line.
>1. rm latency.txt
>2. Run garnet with desired set of parameters. 
>3. Run hist.py (the value of legend can be modified by editing the file)
>>1. hist.py prints all latency values, maximum latency value, minimum latency value.
>>2. Using matplotlib, a histogram is drawn: which shows count of packets at different latency bins.

9 . A list of commands used for building and running garnet are stored in 'list' file: 
```bash
$ cat list
```

10 . To run binary/executable over a multicore setup with garnet network and SE mode: 
```bash
$ scons build/RISCV_MESI_Two_Level/gem5.opt -j9

$ ./build/RISCV_MESI_Two_Level/gem5.opt configs/example/se.py --cpu-type TimingSimpleCPU --num-cpus=64 --l1d_size=16kB --l1i_size=16kB --num-l2caches=64 --l2_size=128kB --num-dirs=64 --mem-size=4096MB --ruby --network=garnet2.0 -c tests/test-progs/hello/bin/riscv/linux/hello
```

11 . To run benchmarks from the LIGRA graph workload suite over a 16-core RISCV system
```bash
$ scons build/RISCV_MESI_Two_Level/gem5.fast -j9

$ ./build/RISCV_MESI_Two_Level/gem5.fast --outdir my_STATS/RISCV_64c_MESI_BFS_L1-1kB_L2-4kB_VC-4_tr-1 configs/example/se.py --cpu-type=TimingSimpleCPU --num-cpus=16 --num-l2caches=16 --l1d_size=1kB --l1i_size=1kB --l1d_assoc=2 --l1i_assoc=2 --l2_size=4kB --l2_assoc=8 --num-dirs=4 --mem-size=4096MB --ruby --network=garnet2.0  --topology=MeshDirCorners_XY --mesh-rows=4 --vcs-per-vnet=4 --router-latency=1 --maxinsts=100000 -c my_benchmarks/ligra/bin/riscv/BFS -o '-n 16 my_benchmarks/ligra/input/rMatGraph_J_5_100'
```

12 . To build your own configuration like 'RISCV_MESI_Two_Level', create a file in 'build' directory and specify the variables CPU_MODELS, PROTOCOL and TARGET_ISA. A description on more per configuration variables is available at : http://www.gem5.org/Build_System


References: 
1. [gem5 documentation](http://gem5.org/Documentation) 
2. [Garnet](http://gem5.org/Garnet) 
3. [Garnet2.0](http://gem5.org/Garnet2.0) 
4. [Garnet2.0 slides](https://pdfs.semanticscholar.org/c1e9/0beac857ce1af1a531b6538804e71efdef05.pdf)  
5. [Interconnection Network](http://www.m5sim.org/Interconnection_Network) 
6. [Tushar_gem5_chips](https://github.com/GT-CHIPS/gem5_chips) 


