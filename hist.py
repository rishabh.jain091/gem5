#!/usr/bin/env python
fname = 'latency.txt'
with open(fname) as f:
  content = f.readlines()

content = [x.strip() for x in content]
lis = []
for x in content:
  lis.append(int(x))

print(lis)

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


minimum = min(lis)
maximum = max(lis)
print("The max latency is : " + str(maximum) + " Cycles")
print("The min latency is : " + str(minimum) + " Cycles")
legend = ['injection rate = 0.01']
bins = maximum - minimum
plt.hist(lis,bins)
plt.xlabel("Latency ")
plt.ylabel("Number of packets")
plt.legend(legend)
plt.show()
